// function 

//1) why function??
//In JavaScript, a function allows you to define a block of code, give it a name and then execute it as many times as you want.

//Normal Function Example.


function function_name(a,b)
{
    return a+b;
}
  //storing in a variable // 
  var container=function_name(5,6);//calling of function 
  console.log(container);//display of function


  /*
  
  what is return value ?
  why we use return inside that function::


  return just throw the value outside of that function where you have return the value.
  */ 

/*

Function Parameters
A function can have one or more parameters, which will be supplied by the calling code and can be used inside a function. JavaScript is a dynamic type scripting language, so a function parameter can have value of any data type.

example 
*/

function ShowMessage(firstName, lastName) {
    alert("Hello " + firstName + " " + lastName);
}

ShowMessage("Steve", "Smith");
ShowMessage("Bill", "Gates");
ShowMessage(100, 200);



//argument object



function ShowMessage(firstName, lastName) {
    alert("Hello " + arguments[0] + " " + arguments[1]);
}

ShowMessage("Steve", "Smith"); 

ShowMessage("Bill", "Gates");

ShowMessage(100, 200);


//function returning function

function multiple(x) {

    function fn(y)
    {
        return x * y;
    }
    
    return fn;
}

var triple = multiple(3);
triple(2); // returns 6
triple(3); // returns 9


//function Expression


var add = function sum(val1, val2) {
    return val1 + val2;
};

var result1 = add(10,20);
var result2 = sum(10,20); // not valid



//Example: Anonymous Function
 showMessage = function (){
    alert("Hello World!");
};

showMessage();

var sayHello = function (firstName) {
    alert("Hello " + firstName);
};

showMessage();

sayHello("Bill");


//Example: Nested Functions


function ShowMessage(firstName)
{
    function SayHello() {
        alert("Hello " + firstName);
    }

    return SayHello();
}

ShowMessage("Smudge");



/*
        POINTS TO REMEMBER 

JavaScript a function allows you to define a block of code, give it a name and then execute it as many times as you want.
A function can be defined using function keyword and can be executed using () operator.
A function can include one or more parameters. It is optional to specify function parameter values while executing it.
JavaScript is a loosely-typed language. A function parameter can hold value of any data type.
You can specify less or more arguments while calling function.
All the functions can access arguments object by default instead of parameter names.
A function can return a literal value or another function.
A function can be assigned to a variable with different name.
JavaScript allows you to create anonymous functions that must be assigned to a variable.

so these are some important point


*/ 

/*
what is promise in js?
The Promise 
object represents the eventual completion (or failure)
 of an asynchronous operation and its resulting value.






*/

// Chained Promises

const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('foo');
    }, 300);
  });
  
  myPromise
    .then(handleResolvedA, handleRejectedA)
    .then(handleResolvedB, handleRejectedB)
    .then(handleResolvedC, handleRejectedC);




    myPromise
.then(handleResolvedA)
.then(handleResolvedB)
.then(handleResolvedC)
.catch(handleRejectedAny);





promise1
.then(value => { return value + ' and bar'; })
.then(value => { return value + ' and bar again'; })
.then(value => { return value + ' and again'; })
.then(value => { return value + ' and again'; })
.then(value => { console.log(value) })
.catch(err => { console.log(err) });
